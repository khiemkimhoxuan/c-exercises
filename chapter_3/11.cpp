#include <iostream>
#include <stdlib.h>

using namespace std;

int humanScore, computerScore;
bool playerTurn = true;


int humanTurn(int humanTotalScore) {
  humanScore += humanTotalScore;
  return humanScore;
  
}

int computerTurn(int computerTotalScore) {
  computerScore += computerTotalScore;
  return computerScore;

}



int main() {
  srand(time(NULL));
  int dice =  0;
  int scoreHuman = 0, scoreComputer = 0;
  while(true) {

    if(humanScore >= 100) {
      cout << "Player wins with score: " << humanScore << endl;
      break;
    }else if(computerScore >= 100) {
      cout << "Computer wins with score: "<< computerScore  << endl;
      break;
    }
    
    
    if(!playerTurn) {
      if(scoreComputer >= 20) {
	cout << "summing up points! Total earned: " << scoreComputer<< endl;
	computerTurn(scoreComputer);
	playerTurn = true;
	scoreComputer = 0;
      }else {
	dice = rand() % 6 + 1;
	if(dice == 1) {
	}else {
	  scoreComputer += dice;
	  cout << "Dice roll: " << dice<<", Scores collected: " << scoreComputer << endl;
	}
      }
      

    }else {
      cout << "Players turn: ";
      char choice;
      cin >> choice;
      
      if(choice == 'r') {
	dice = rand() % 6 + 1;
	if(dice == 1) {
	  playerTurn = false;
	  scoreHuman = 0;
	  cout << "Computers turn: " << endl;
	}else {
	  scoreHuman += dice;
	  cout << "Dice roll: " << dice <<", Scores collected: " <<scoreHuman<< endl;

	}



      }else if(choice == 'h') {
	cout << "summing up points! Total earned: " << scoreHuman<< endl;
	humanTurn(scoreHuman);
	playerTurn = false;
      }
      
    }

  }


  return 0;
}
