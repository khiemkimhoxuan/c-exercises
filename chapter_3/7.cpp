#include <iostream>
#include <cmath>

using namespace std;

static int windChillIndex(double v, int t) {
return 33-((10*sqrt(v)-v+10.5)*(33-t)/23.1);

}

int main() {
	double v, t, w;

	while (true) {
		cout << "Enter wind speed: ";
		cin >> v;

		cout << "Enter temperature: ";
		cin >> t;

		if (t > 10) {
			cout << "Temperature cannot be under 10, try again: " << endl;
		} else {
			w = windChillIndex(v,t);
			cout << "The wind chill index is: " << w << endl;
		}
	}

	return 0;
}