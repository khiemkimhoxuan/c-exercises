#include <iostream>

using namespace std;

static double estimateInflationRate(double today, double yearAgo) {
	double inflationRate = (today-yearAgo)/yearAgo * 100.0;
	return inflationRate;
}


int main() {
	double yearAgo, today;
	
	while(true) {
		cout << "Price of item a year ago: ";
		cin >> yearAgo;
	
		cout << "Price of item today: ";
		cin >> today;
	
		cout << "Estimated inflation rate: " << estimateInflationRate(today,yearAgo) << "%" <<endl;
	}
	
	return 0;
}