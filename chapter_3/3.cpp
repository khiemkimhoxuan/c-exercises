#include <iostream>

using namespace std;

static double estimateInflationRate(double today, double yearAgo) {
	double inflationRate = (today-yearAgo)/yearAgo * 100.0;
	return inflationRate;
}

static void costEstimationOfItem(double currentPrice, double inflationRate) {
	for(int i = 1; i <= 2; i++) {
		cout << "The estimation cost for " << (i) << ".year is: " << (((inflationRate/100) * i + 1) * currentPrice) << endl;
	}
}


int main() {
	double yearAgo, today;
	
	while(true) {
		cout << "Price of item a year ago: ";
		cin >> yearAgo;
	
		cout << "Price of item today: ";
		cin >> today;
	
		cout << "Estimated inflation rate: " << estimateInflationRate(today,yearAgo) <<"%" << endl;
		costEstimationOfItem(today,estimateInflationRate(today,yearAgo));
	}
	
	return 0;
}