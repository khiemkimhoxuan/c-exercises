#include <iostream>
#include <stdlib.h>
#include <ctime>
#include <cstdlib>
#include <random>
//remember to include -std=c++0x when compiling with these libraries above


using namespace std;

class Dice {

public:
	int values[6];

	Dice() {
		values[0] = 1;
		values[1] = 2;
		values[2] = 3;
		values[3] = 4;
		values[4] = 5;
		values[5] = 6;
	}

};

static int simRollTwoDice(Dice d[]) {

	int res = 0;

	int r1 = rand() % 5 + 1;
	int r2 = rand() % 5 + 1;
	res += d[0].values[r1] + d[1].values[r2];


	return res;
}

int main() {
	Dice dices[2];

	double numWins = 0.0, numLoss = 0.0;
	srand( time(NULL));
	int result = 0;
	for(int i = 0; i < 10000; i++) {
		result = simRollTwoDice(dices);
		
		if(result == 7 || result == 11)
			numWins++;
		else if(result == 2 || result == 3 || result == 12)
			numLoss++;
		

	}
	double prop = static_cast<double>((numWins/(numWins+numLoss)));
	cout << "Probability of winning: " << prop*100 << endl;
	cout << "number of wins: " << numWins << endl;
	cout <<"number of loss:" << numLoss << endl;

	return 0;
}
