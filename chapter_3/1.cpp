#include <iostream>
#define LITERS_PER_GALLON 0.264179

using namespace std;

double produceNumberOfMilesPerGallon(double l, double miles) {
	
	double milesPerGallon;
	
	milesPerGallon = miles / (l *LITERS_PER_GALLON);
	return milesPerGallon;
	
}


int main() {
	double liters, numberOfMiles;
	while(true) {
		cout << "enter liters of gasoline: ";
		cin >> liters;
	
		cout << "enter number of miles traveled: ";
		cin >> numberOfMiles;
	
	
		cout << "miles per gallon: " << produceNumberOfMilesPerGallon(liters,numberOfMiles) << endl;
	}
	
	return 0;
}