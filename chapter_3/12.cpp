    #include <iostream>
    #include <cmath>
    #include <string>
    using namespace std;
    void getInput (int &, int &, int &);
    int getCenturyValue (int);
    int getYearValue (int);
    int getMonthValue (int, int);
    void getInput(int &month, int &day, int &year)
    {
    cout << "Enter month: ";
    cin >> month;
    cout << "Enter day: ";
    cin >> day;
    cout << "Enter year: ";
    cin >> year;
    }
    int main()
    {
    int month, mth, yr, cen, day, year, dow;
    string name;
    getInput (month, day, year);
    mth = getMonthValue(month, year);
    yr = getYearValue(year);
    cen = getCenturyValue(year);
    dow = (day + mth + yr + cen) % 7;
    switch(dow)
    {
    case 0:
    name = "Sunday";
    break;
    case 1:
    name = "Monday";
    break;
    case 2:
    name = "Tuesday";
    break;
    case 3:
    name = "Wednesday";
    break;
    case 4:
    name = "Thursday";
    break;
    case 5:
    name = "Friday";
    break;
    case 6: name = "Saturday";
    break;
    }
    cout << month << "/" << day << "/" << year <<" is a " << name <<endl;
    system("pause");
    return 0;
    }
    bool isLeapYear (int year)
    {
    if( (year % 400 == 0) || (( year % 4 == 0) && (year % 100 !=0) ))
    return true;
    else
    return false;
    }
    int getCenturyValue (int y)
    { int c, r;
    c=y / 100;
    r=(c % 4);
    return (3 - r) *2;
    }
    int getYearValue (int y)
    {
    int yv;
    yv = y % 100;
    return yv + (yv / 4);
    }
    int getMonthValue (int m, int y)
    {
    switch(m - 1)
    {
    case 0:
    if (isLeapYear(y))
    return 6;
    else
    return 0;
    case 1:
    if (isLeapYear(y))
    return 2;
    else
    return 3;
    case 2:
    case 10:
    return 3;
    case 3:
    case 6:
    return 6;
    case 4:
    return 1;
    case 5:
    return 4;
    case 7:
    return 2;
    case 11:
    case 8:
    return 5;
    case 9:
    return 0;
    }
    system("pause");
    return 0;
    }
