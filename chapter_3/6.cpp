#include <cstdlib>
#include <iostream>
#include <cmath>
#include <string>
#include <stdlib.h>

using namespace std;

static bool checkExit(string word) {
	return word == "exit";
}

static double averageDeviation(double s1, double s2, double s3, double s4) {
	return ((s1+s2+s3+s4)/4);
}

static double standardDeviation(double s1, double s2, double s3, double s4, double avg) {
	double sum = s1 + s2 + s3 + s4;
	return sqrt(pow((sum - avg),2));
}

static void computeStats(double s1, double s2, double s3, double s4, double a, double stdev) {
	cout << "Average Deviation: " << a << endl;
	cout << "Standard Deviation: " << stdev << endl;
	
}

static void menuLoop() {
	
	string word;
	double s1,s2,s3,s4;
	while(true) {
		cout << "Enter score 1: ";
		cin >> word;
			s1 = atof(word.c_str());
			cout << "Enter score 2 : ";
			cin >> word;
			s2 = atof(word.c_str());
			cout << "Enter score 3: ";
			cin >> word;
			s3 = atof(word.c_str());
			cout << "Enter score 4: ";
			cin >> word;
			s4 = atof(word.c_str());
			
			//do a check for each later
			double a = averageDeviation(s1,s2,s3,s4);
			double s = standardDeviation(s1,s2,s3,s4,a);
			

			computeStats(s1,s2,s3,s4,a,s);
			cout << "Continue? (exit)";
			cin >> word;
			if(checkExit(word)) 
				exit(1);
		
	}
}




int main() {
	
	menuLoop();
	
	return 0;
}