#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <stdlib.h>
#include <ctype.h>
#include <vector>
#include <cctype>
#include <string>
#include <sstream>
#include <algorithm>

using namespace std;

inline bool isInteger(const std::string & s) {
    if (s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+')))
        return false;

    char * p;
    strtol(s.c_str(), &p, 10);

    return (*p == 0);
}

string digitLookup[3][10] = {
    {"Zero", "One", "Two", "Three", "Four", "Five", "Six",
        "Seven", "Eight", "Nine"},
    {"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen",
        "Sixteen", "Seventeen", "Eighteen", "Nineteen"},
    {"", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"}

};

string exchangeNumberToString(int number) {
    //num/10 = tens digit
    // num%10 -> ones digit
    string s("");
    if (number > 19) {
        s = digitLookup[2][number / 10];
        if ((number % 10) > 0) {
        	string tl = digitLookup[0][number % 10];
        	transform(tl.begin(),tl.end(),tl.begin(),::tolower);
            s += (string("-") + string(tl));
        }
    }else if (number > 9) {
          s = digitLookup[1][number - 10];
    } else {
        s = digitLookup[0][number];
    }


    return s;

}

int main() {
    fstream inputStream;
    inputStream.open("bottles.txt");
    string text;

    vector<string> lyrics;
    vector<string>::iterator it;

    while (!inputStream.eof()) {
        inputStream >> text;
        if (isInteger(text)) {
            //replace the text with a digit
            string s = exchangeNumberToString(atoi(text.c_str()));
            cout << s <<" ";
        }else {
        	cout << text << " ";
        }
        
    }

    inputStream.close();
    return 0;
}