#include <iostream>
#define WEIGHT(km) km * 2.2
#define HEIGHT(cm) cm * 0.393701
#define INCH_EACH_10YEARS (1/8)
#define INCH_EACH_2YEARS (1/10)


using namespace std;

static double hatSize(double h, double w) {
	return WEIGHT(w)/HEIGHT(h) * 2.9;
}

static double jacketSize(double h, double w, int age) {
	double jsize = (HEIGHT(h) * WEIGHT(w)) / 288;
	
		for(int i = age; i > 30; i-= 10) {
			if(i % 10 == 0) {
				jsize += 0.125;
			}
	}
	
	
	return jsize;
}

static double waistInInches(double weight, double age) {
	double wInch = weight / 5.8;
	
	for(int i = age; i > 28; i -= 2) {
		if(i % 2 == 0) 
			wInch += 0.1;
	}
	
	return wInch;
}


int main() {
	double height, weight;
	int age;
	cout << "Enter height (cm): ";
	cin >> height;
	
	cout << "Enter weight (kg): ";
	cin >> weight;
	
	cout << "Age?: ";
	cin >> age;
	
	cout << "Hat size: " << hatSize(height,weight) << endl;
	cout << "Jacket Size: " << jacketSize(height,weight,age) << endl;
	cout << "Waist size: " << waistInInches(weight,age) << endl;
	
	
	
	
	return 0;
}