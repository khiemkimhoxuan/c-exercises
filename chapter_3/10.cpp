#include <iostream>

using namespace std;

static int calculateHeight(char gender, int hf, int hm, int hff, int mff) {
	int heightChild = 0;
	if(hff > 0 && mff > 0) {
		hf += (hff * 12);
		hm += (mff * 12);
	}
	char g = tolower(gender);
	if(g == 'm') {
	  heightChild = ((hm * (13/12)) + hf)/2;

	}else if(g == 'f') {
	  heightChild = ((hf * (12/13))+ hm)/2;
	}


	
	
	
	
	
	
	
	return heightChild;
}


int main() {
	
	int heightFatherInch = 0;
	int heightMotherInch = 0;
	int heightFatherFeet = 0;
	int heightMotherFeet = 0;
	
	char childGender;
	
	string picker;
	
	while(true) {
		cout << "Enter inch or feet (to exit, type exit): ";
		cin >> picker;
		
		if(picker == "inch") {
			cout << "Height of father: ";
			cin >> heightFatherInch;
			cout << "Height of mother: ";
			cin >> heightMotherInch;
		}else if(picker == "feet") {
			cout << "Height of father: (feet first then inch)";
			cin >> heightFatherFeet;
			cin >> heightFatherInch;
			cout << "Height of mother: (feet first then inch)";
			cin >> heightMotherFeet;
			cin >> heightMotherInch;
		}else if(picker == "exit") {
			exit(1);
		}
		
		cout << "Enter gender of child: ";
		cin >> childGender;
		int resultHeight = calculateHeight(childGender,heightFatherInch, heightMotherInch, heightFatherFeet,heightMotherFeet);
		cout << "The childs height is: " << (resultHeight/12) << "feet and " << (resultHeight%12) << " inches"  << endl;
		
		
		
	}
	
	
	
	return 0;
}
