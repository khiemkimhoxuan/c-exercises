#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;
#define CONSTANT_UNIVERSAL_GRAVITATION 6.673 * pow(10,-8) // in meters, not cm


static double gravitationalForce(double m1, double m2, double d) {
	return (CONSTANT_UNIVERSAL_GRAVITATION * m1 * m2)/pow(d,2);
}

int main() {
	double m1,m2,d;
	
	while(true) {
		cout << "Body mass m1: ";
		cin >>  m1;
		cout << "Body mass m2: ";
		cin >> m2;
	
		cout << "Distance: ";
		cin >> d;
	
		//one dyne is g *cm/sec^2
	
		double F = gravitationalForce(m1,m2,d);
		cout << "The gravitationalForce is: " << setprecision(4)<<  F << " dynes" <<  endl;
	}
	
	return 0;
}