#include <iostream>
#include <stdlib.h>
#include <string>


using namespace std;


void input(double& meters) {
	string input;
	cout << "Command: (exit, start): ";
	cin >> input;
	if(input == "start") {
		cout << "Enter meters: ";
		cin >> meters;
	}else if(input == "exit") {
		exit(1);
	}
}

void calcMetersToFoot(int& foot, int& inch, double& height) {
	double convertHeight = (height/0.3048) / 100;
	cout << convertHeight << endl;
	foot = (int) convertHeight;
}



int main() {
	int foot, inch;
	double height;
	input(height);
	calcMetersToFoot(foot,inch,height);
	cout << "feet: " << foot << " inches: " << inch << endl;
	return 0;
}
