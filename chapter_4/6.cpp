#include <iostream>
#include <stdlib.h>
#include <string>
#include <math.h>

using namespace std;

void input(double& meters) {
	string input;
	cout << "Command: (exit, start): ";
	cin >> input;
	if (input == "start") {
		cout << "Enter meters: ";
		cin >> meters;
	} else if (input == "exit") {
		exit(1);
	}
}

void calcMetersToFoot(int& foot, int& inch, double& height) {
	double convertHeight = (height / 0.3048) / 100;
	cout << convertHeight << endl;
	foot = (int) convertHeight;
}

void input(int& foot, int& inch) {
	string input;
	cout << "Command: (exit, start): ";
	cin >> input;
	if (input == "start") {
		cout << "Enter foot: ";
		cin >> foot;
		cout << "Enter inch: ";
		cin >> inch;
	} else if (input == "exit") {
		exit(1);
	}
}

void calcFootInchToMeters(int& foot, int& inch, double& height) {
	double inchConvert = (double) (inch / 12.0);
	cout << "Inches: " << inchConvert << endl;
	double footConvert = (foot * 0.3048);
	footConvert += (inchConvert * 0.3048);

	height = footConvert * 100;

}

int main() {
	int choice;
	int foot, inch;
	double height;
	while (true) {
		cout << "1 for feet 2 meter or 2 for meter 2 feet :" << endl;
		cin >> choice;

		if (choice == 1) {
			input(foot, inch);
			calcFootInchToMeters(foot, inch, height);
			cout << "Height in centimeters: " << height << endl;
			cout << "Height in meters: " << height/100 << endl;

		} else if (choice == 2) {
			input(height);
			calcMetersToFoot(foot,inch,height);
			cout << "feet: " << foot << " inches: " << inch << endl;
			
		}
	}

	return 0;
}