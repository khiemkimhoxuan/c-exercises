#include <iostream>
#include <stdlib.h>
#include <string>


using namespace std;


void input(int& foot, int& inch) {
	string input;
	cout << "Command: (exit, start): ";
	cin >> input;
	if(input == "start") {
		cout << "Enter foot: ";
		cin >> foot;
		cout << "Enter inch: ";
		cin >> inch;
	}else if(input == "exit") {
		exit(1);
	}
}

void calcFootInchToMeters(int& foot, int& inch, double& height) {
	double inchConvert = (double)(inch / 12.0);
	cout << "Inches: " << inchConvert << endl;
	double footConvert = (foot * 0.3048);
	footConvert += (inchConvert * 0.3048);

	height = footConvert * 100;

}



int main() {
	int foot, inch;
	double height;
	input(foot,inch);
	calcFootInchToMeters(foot,inch,height);
	cout << "Height in centimeters: " << height << endl;
	
	return 0;
}
