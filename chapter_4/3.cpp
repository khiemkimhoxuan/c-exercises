#include <iostream>
#include <stdlib.h>

using namespace std;

void computeCoin(int coinValue, int& number, int& amountLeft);
bool validateInput(int coinValue, int& amountLeft);

int main() {
	int number = 0;
	int amountLeft;
	string input;
	//coin value 25 cents, 10 cents and 1 cent
	while (true) {
		cout << "Enter amount (exit): ";
		cin >> input;

		if (input == "exit")
			exit(1);
		else
			amountLeft = atoi(input.c_str());
		computeCoin(25, number, amountLeft);

		cout << "number: " << number << " amount left: " << amountLeft << endl;
		number = 0;
		computeCoin(10,number,amountLeft);
		cout << "number: " << number << " amount left: " << amountLeft << endl;
		number = 0;
		computeCoin(1,number,amountLeft);
		cout << "number: " << number << " amount left: " << amountLeft << endl;

	}

	return 0;
}

void computeCoin(int coinValue, int& number, int& amountLeft) {
	
	if (validateInput(coinValue, amountLeft)) {
		number += (amountLeft / coinValue);
		amountLeft %= coinValue;

	} else {
		cout << "Invalid!" << endl;
	}
}

bool validateInput(int coinValue, int& amountLeft) {
	return 0 < coinValue && amountLeft > 0 && amountLeft < 100;
}

