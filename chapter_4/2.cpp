#include <iostream>
#include <string>
#include <stdlib.h>
#include <cmath>

using namespace std;

bool validateTriangleInput(double a, double b, double c) {
	if (a + b > c && a + c > b && b + c > a) {
		return true;
	}
	return false;
}

void calcAreaPerim(double a, double b, double c, double& area,
		double& perimeter) {
	double s;
	perimeter = (a + b + c);
	s = perimeter / 2;
	area = sqrt(s * (s - a) * (s - b) * (s - c));
}

int main() {
	double a, b, c, area, perimeter;
	double semiperimeter;
	while (true) {
		
		
		
		cout << "Enter a: ";
		cin >> a;
		cout << "Enter b: ";
		cin >> b;
		cout << "Enter c: ";
		cin >> c;
		if (validateTriangleInput(a, b, c)) {
			calcAreaPerim(a, b, c, area, perimeter);
			semiperimeter = perimeter / 2;
			cout << "Area is: " << area << ", While semiperimeter is: "
					<< semiperimeter << endl;
		} else {
			cout << "Invalid, please try again!" << endl;
		}

	}

	return 0;
}