#include <iostream>
#include <stdlib.h>
#include <string>
#include <stdio.h>
#include <string.h>

using namespace std;

bool validateTime(int hour, int min) {
	if(hour <= 12 || hour <= 23)
		if(min >= 0 || min <=59) 
			return true;
	
	return false;
}

void input(int &hour, int& min) {
	string s;
	cout << "Enter time: (hh:mm) ";
	cin >> s;
	
	if(s == "exit") {
		exit(0);
	}
	
	char delimeter = ':';
	string token1 = s.substr(0, s.find(delimeter));
	string token2 = s.substr(s.find(delimeter) + 1, s.length());
	hour = atoi(token1.c_str());
	min = atoi(token2.c_str());
	
	if(!validateTime(hour,min)) {
		cout << "Wrong validated time, Type in again";
	}
	

}
void output(char c, int hour, int min) {
	string s;
	if (c == 'A') {
		s = "AM";
	} else if (c == 'P') {
		s = "PM";
	}
	cout << hour << ":" << min << s << endl;

}
void convertAmOrPm(char& c, int& hour) {
	if(hour > 12) {
		hour %= 12;
		c = 'P';
	}else {
		if(hour == 12) {
			c = 'P';
			return;
		}
		if(hour == 0) 
			hour = 12;
		
		c = 'A';
		
	}

}

int main() {
	int hour, min;
	char c;
	while (true) {
		input(hour, min);
		convertAmOrPm(c, hour);
		output(c, hour, min);
	}

	return 0;

}