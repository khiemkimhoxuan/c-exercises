#include <iostream>
#include <stdlib.h>
#include <iomanip>

using namespace std;

int main() {
	const double WAGE_PER_HOUR = 16.78;
	const int REGULAR_WORKING_HOUR_WEEK = 40;
	const double OVERTIME_WORKING_PAY = 1.5;
	int dependents;

	int hoursWorked;
	int netTakeHomePay;
	cout << "Enter hours worked: ";
	cin >> hoursWorked;

	if (hoursWorked > 40) {
		netTakeHomePay = hoursWorked * WAGE_PER_HOUR * OVERTIME_WORKING_PAY;
	} else {
		netTakeHomePay = hoursWorked * WAGE_PER_HOUR;
	}

	cout << "Enter any dependents: ";
	cin >> dependents;

	//deduce the income with the taxes.
	cout << "The gross salary: " << netTakeHomePay << endl;
	cout << "Social security tax (6%): " << (netTakeHomePay * 0.06) << endl;
	cout << "Federal income tax (14%): " << (netTakeHomePay * 0.14) << endl;
	cout << "State income tax (5%): " << (netTakeHomePay * 0.05) << endl;

	cout << "10$ deduced per week for union dues" << endl;
	netTakeHomePay -= 10;

	if (dependents > 2) {
		//deduce 35$ from the variable
		netTakeHomePay -= 35;
		cout << "35$ deduced to cover health insurance" << endl;
	}

	cout << "Net Take Home Pay for the week: " << netTakeHomePay << endl;

	return 0;
}