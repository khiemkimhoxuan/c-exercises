#include <iostream>
#include <stdlib.h>
#include <limits>
#include <iomanip>

using namespace std;


int main() {
	double const ONE_TENTH_SWEETENER = 0.001;
	double noOfArtSweeteners,weightMouse,weightDieter;
	
	int numberOfCans;
	
	
	cout << "Enter the amount of artificial sweeteners: ";
	cin >> noOfArtSweeteners;
	
	cout << "Enter the weight of the mice ";
	cin >> weightMouse;
	
	cout << "Enter the dieters weight ";
	cin >> weightDieter;
	
	double sweetenerDieter = (noOfArtSweeteners/weightMouse)*weightDieter;
	numberOfCans = weightDieter/ONE_TENTH_SWEETENER;
	
	cout << "Number of cans of diet soda to kill a person is: "  << numberOfCans << endl;
	
	return 0;
	
}