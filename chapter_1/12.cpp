#include <iostream>

using namespace std;

int main() {
	int pounds = 110; 
	int add_pounds = 5;
	int inch;
	int add_inch;
	
	cout << "Enter inches: ";
	cin >> inch;
	
	cout << "Enter additional inches: ";
	cin >> add_inch;
	
	 int ideal = 110 + (add_inch * add_pounds);
	cout << "Ideal body weight: " << ideal << endl;
	
	return 0;
}