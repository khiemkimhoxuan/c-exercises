#include <iostream>
#include <math.h>
#define FEET_PER_SEC 32

using namespace std;

int main() {
	
	const double acceleration = FEET_PER_SEC;
	
	double time;
	
	cout << "Enter time in seconds: ";
	cin >> time;
	
	
	double distance = 0.5 * acceleration * pow(time,2.0);
	
	cout << "Distance calculated as: " << distance << endl;
	
	return 0;
}