#include <iostream>
#include <iomanip>
#include <limits>
#include <stdlib.h>
#include <stdio.h>

using namespace std;


int main() {
	
	int MaxCapacityRoom;
	int numberOfPeopleToAttend;
	
	cout << "Enter room capacity: ";
	cin >> MaxCapacityRoom;
	cout << "Enter number of people attending: ";
	cin >> numberOfPeopleToAttend;
	
	if(numberOfPeopleToAttend <= MaxCapacityRoom) {
		cout << "It is legal to hold meeting and additional people that can attend is: " << (MaxCapacityRoom - numberOfPeopleToAttend) << endl;
	}else 
		cout << "Metting room cannot be held due to fire regulations.. People that needs to be excluded in amount are: " << (numberOfPeopleToAttend - MaxCapacityRoom) << endl;
	
	
	return 0;
}