#include <iostream>

using namespace std;

int main() {

	int seconds;
	cout << "Enter seconds: ";
	cin >> seconds;

	int hour, min, sec;
	int restHour;
	int modHour;

	hour = seconds / 3600;
	restHour = seconds % 3600;

	min = restHour / 60;
	sec = restHour % 60;
	
	modHour = (hour%24);

	cout << modHour << ":" << min << ":" << sec << endl;

	return 0;
}