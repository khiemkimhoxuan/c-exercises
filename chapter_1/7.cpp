#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <stdio.h>

using namespace std;


int main() {
	double weightInPounds, weightInKilograms;
	double burnedCalories;
	int METS;
	double numMinSpent;
	
	cout << "Enter weight in pounds: ";
	cin >> weightInPounds;
	
	weightInKilograms = (weightInPounds/2.2);
	
	cout << "Enter number of METS: ";
	cin >> METS;
	
	cout << "Enter number of minutes spent: ";
	cin >> numMinSpent;
	
	burnedCalories = 0.0175 * METS * weightInKilograms * numMinSpent;
	
	cout << "Number of burned calories are: " << burnedCalories <<"kcal" <<endl;
	
	
	return 0;
}