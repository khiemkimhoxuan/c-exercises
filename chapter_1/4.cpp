#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <limits>
#include <iomanip>

using namespace std;

int main() {
	double consumerAmount, interestRate;
	int loanDuration;

	cout << "Enter amount of interest: ";
	cin >> consumerAmount;

	cout << "Enter interest rate (%): ";
	cin >> interestRate;

	cout << "Enter loan duration in month: ";
	cin >> loanDuration;

	interestRate /= 100;
	double monthInYears = loanDuration / 12.0;

	double faceValue = (consumerAmount * interestRate) * monthInYears;
	double leftForConsumer = consumerAmount - faceValue;
	double monthlyLoan = consumerAmount / loanDuration;

	cout << "The interest: " << faceValue << endl;
	cout << "The leftover for the consumer: " << leftForConsumer << endl;
	cout << "The monthly payment: " << setprecision(4) << monthlyLoan << endl;

	return 0;
}