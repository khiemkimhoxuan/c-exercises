#include <iostream>
#include <iomanip>
#include <limits>

using namespace std;


int main() {
	
	cout << "Enter weight of a breakfast serial: ";
	double oz;
	double metric_tons;
	cin >> oz;
	
	metric_tons = oz * 0.00002834952;
	
	cout << "The metric ton of breakfast serial is: " << setprecision(2) << metric_tons << endl;
	
	
	
	return 0;
}