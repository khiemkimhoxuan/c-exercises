#include <iostream>
#include <stdlib.h>
#include <limits>

using namespace std;

int main() {
	const double PAY_INCREASE = (7.6/100);
	
	double annSalary,retroActivePay,newAnnualSalary,monthlySalary,newMonthlySalary;
	
	cout << "Enter the employee's previous annual salary: ";
	cin >> annSalary;
	
	
	newAnnualSalary= (annSalary * PAY_INCREASE) + annSalary;
	monthlySalary = annSalary/12;
	newMonthlySalary = newAnnualSalary/12;
	
	retroActivePay = newMonthlySalary - monthlySalary;
	
	
	cout << "Retroactive pay for employee: " << retroActivePay << endl;
	cout << "New Annual Salary: " << newAnnualSalary << endl;
	cout << "New monthly salary: " << newMonthlySalary << endl;
	
	
	return 0;
}