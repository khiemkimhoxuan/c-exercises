#include <iostream>
#include <stdlib.h>

using namespace std;

int main() {
	
	int number_coupons;
	int number_candybars = 0;
	int number_gumballs = 0;
	cout << "Number of coupons? ";
	cin >> number_coupons;
	
	while(number_coupons > 0 && number_coupons > 9) {
		number_coupons -= 10;
		number_candybars++;
	}
	
	while(number_coupons > 0 && number_coupons > 2) {
		number_coupons -= 3;
		number_gumballs++;
	}
	
	cout << "Number of coupons left: " << number_coupons << endl;
	cout << "Number of candybars to get: " << number_candybars << endl;
	cout << "Number of gumballs to get: " << number_gumballs << endl;
	
	
	
	return 0;
}