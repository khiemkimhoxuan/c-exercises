#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <limits>

using namespace std;

int main() {

	int n;
	double guess;
	double r;

	cout << "Type in square root of a positive number: ";
	cin >> n;

	guess = n/2;
	for (int i = 0; i < 5; i++) {
		r = (n/guess);
		guess = (guess + r)/2;
	}
	
	cout << "Answer: " << setprecision(4) << guess << endl;

	return 0;
}