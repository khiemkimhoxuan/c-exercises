#include <iostream>
#include <string>
#include <stdlib.h>
#include <string.h>
#include <map>

using namespace std;

char vowels[] = "aeiouy";
map<char, int> vowelCheck;

bool exists(char c) {
	map<char, int>::iterator pos = vowelCheck.find(c);
	if (pos == vowelCheck.end()) {
		return false;
	}
	return true;
}

int getKey(char c) {

	int key = -1;

	for (map<char, int>::iterator it = vowelCheck.begin(); it
			!= vowelCheck.end(); ++it) {
		if (it->first == c) {
			key = it->second;
			break;
		}
	}
	return key;

}

int main() {

	string word;
	int sumVowel;
	cout << "enter a string!: ";

	getline(cin, word);

	for (int i = 0; i < word.length(); i++) {
		//check if a character is in a vowel
		if (strchr(vowels, word[i])) {

			if (exists(word[i])) {
				int update = getKey(word[i]);
				update++;
				vowelCheck[word[i]] = update;
			} else {
				int count = 1;
				vowelCheck.insert(make_pair(word[i], count));
			}
		}
	}

	sumVowel = 0;
	for (map<char, int>::iterator it = vowelCheck.begin(); it
			!= vowelCheck.end(); ++it) {
		
		cout << "Vowel : " << it ->first;
		cout << " counted: " << it->second << endl;
		sumVowel += it->second;
	}

	cout << "Sum Vowels found: " << sumVowel << endl;


	return 0;
}