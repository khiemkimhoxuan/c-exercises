#include <iostream>
#include <stdlib.h>
#include <string>

using namespace std;

int main() {
	bool flag = false;
	string word;
	
	cout << "Enter a word: ";
	getline(cin,word);
	
	for(int i = 0, j = word.length()-1; i <=
	(word.length()/2); ++i, --j) {
		if(word[i] == word[j])
			flag = true;
		
	}
	
	if(flag) 
		cout << "Palindrome!" << endl;
	else
		cout << "Not Palindrome!" << endl;
	
	return 0;
}