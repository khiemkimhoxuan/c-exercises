#include <iostream>
#include <stdlib.h>
#include <string>

using namespace std;

string createPigLatin(string s) {
	string returnString;

	for (int i = 1; i < s.length(); i++) {
		returnString += s[i];
	}
	returnString += "-";
	returnString += s[0];
	returnString.append("jay");

	return returnString;

}

int main() {

	string word;
	cout << "Enter a word: ";
	getline(cin, word);

	string newWord = createPigLatin(word);
	cout << newWord << endl;

}