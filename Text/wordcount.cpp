#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <map>
#include <vector>

using namespace std;

map<string, int> wordCounter;

vector<string> splitString(string input, string delimiter) {
	vector<string> output;
	size_t start = 0;
	size_t end = 0;

	while (start != string::npos && end != string::npos) {
		start = input.find_first_not_of(delimiter, end);

		if (start != string::npos) {
			end = input.find_first_of(delimiter, start);

			if (end != string::npos) {
				output.push_back(input.substr(start, end - start));
			} else {
				output.push_back(input.substr(start));
			}
		}
	}

	return output;

}

static bool exists(string word) {
	//iterate and find out if the word exist
	map<string, int>::iterator pos = wordCounter.find(word);
	if (pos == wordCounter.end()) {
		return false;
	}

	return true;

}

int getValue(string w) {

	int value = -1;

	for (map<string, int>::iterator it = wordCounter.begin(); it
			!= wordCounter.end(); ++it) {
		if (it->first == w) {
			value = it->second;
			break;
		}
	}
	return value;

}

static void splitToken(string wordLine) {
	string delimiter = " ,.:\t\n";
	string token;
	ssize_t pos = 0;
	//use map to store the tokens
	vector<string> words = splitString(wordLine, delimiter);
	for(std::vector<string>::size_type i = 0; i != words.size(); i++) {
	    token = words[i];
		if (exists(token)) {
			int update = getValue(wordLine);
			update++;
			wordCounter[token] = update;
		} else {
			wordCounter[token] = 1;
		}
		
		
	}
	
	/*while ((pos = wordLine.find(delimeter) != string::npos)) {
		token = wordLine.substr(0, pos);
		if (exists(token)) {
			int update = getValue(wordLine);
			update++;
			wordCounter[token] = update;
		} else {
			wordCounter[token] = 1;
		}
		wordLine.erase(0, pos + delimeter.length());

	} */

}

static void readFile(char *filename) {
	ifstream myfile(filename);
	string line;
	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			splitToken(line);
		}

		myfile.close();

	} else {
		cout << "File error, unable to open the file" << endl;
	}
}

static void printWordCounter() {

	int summary = 0;
	cout << "GENERAL STATISTIC: " << endl;
	for (map<string, int>::iterator it = wordCounter.begin(); it
			!= wordCounter.end(); it++) {
		cout << it->first << ": " << it->second << endl;
		summary += it->second;

	}
	cout << "Words total in the textfile: " << summary << endl;
}

int main(int argc, char **argv) {

	readFile(argv[1]);
	printWordCounter();
	return 0;
}