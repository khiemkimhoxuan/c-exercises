#include <iostream>
#include <stdlib.h>
#include <string>
using namespace std;

string reverseString(string s) {
	int startIndex = 0;
	int endIndex = s.length() - 1;

	for (int i = 0; i < s.length() / 2; i++) {
		char tmp = s[startIndex];
		s[startIndex] = s[endIndex];
		s[endIndex] = tmp;
		startIndex++;
		endIndex--;
	}

	return s;

}

int main() {

	//Reverse a string,
	cout << "Write a string word: ";
	string word;
	getline(cin, word);

	string newString = reverseString(word);
	cout << newString << endl;

	return 0;

}