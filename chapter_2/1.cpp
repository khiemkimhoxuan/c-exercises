#include <iostream>
#include <stdlib.h>
#include <iomanip>

using namespace std;


#define CONVERT(d) (d/100)


int main() {
	double costOfItem;
	int years;
	double rate;
	
	cout << "What is the cost of the item: ";
	cin >> costOfItem;
	
	cout << "Number of years when the item was purchased: ";
	cin >> years;
	
	cout << "Enter rate of inflation: ";
	cin >> rate;
	
	
	
	double storeRates = 0;
	double priceAdjustments;
	for(int i = 0; i < years; i++) {
		double convertRate = CONVERT(rate * (i+1)) + 1;
		priceAdjustments = (costOfItem * (convertRate));
		
		cout << (i+1) << " year increase rate: " << priceAdjustments << endl;
		
	}
	
	
	
	return 0;
}