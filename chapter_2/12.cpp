#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <iostream>
#include <cctype>

using namespace std;



int main() {
	map<char,int> frequencies;
	map<char,int>::iterator it;
	
	size_t totalNumbers = 0;
	
	fstream inputStream;
	
	inputStream.open("numbers.txt");
	string strNum;
	while(!inputStream.eof()) {
		getline(inputStream,strNum);
		if(isdigit(strNum[0])) {
		totalNumbers++;
		frequencies[strNum[0]]++;
		
		
		}
			totalNumbers -= frequencies['0'];
				
		
	}
	
		
		for(it = frequencies.begin(); it != frequencies.end(); it++) {
			cout << "Digit: " << it->first << ", total: " << it -> second << " time(s). With " << 
			float(it->second*100 / totalNumbers) <<"%s" << endl;
			
		}
		
		
		inputStream.close();
	
	
	
	return 0;
}