#include <iostream>
#include <iomanip>

using namespace std;

int main() {
	double scoresEachExercise = 0.0, totalPoints = 0.0;
	int Nexercise, NscoreEach, NtotalPoints;

	cout << "How many exercises to input? ";
	cin >> Nexercise;

	for (int i = 0; i < Nexercise; i++) {
		cout << "Score received for exercise " << (i + 1) << ": ";
		cin >> NscoreEach;

		cout << "Total points possible for exercise " << (i + 1) << ":";
		cin >> NtotalPoints;

		//sum up the value

		scoresEachExercise += NscoreEach;
		totalPoints += NtotalPoints;
	}
	
	double percent = double(scoresEachExercise/totalPoints);

	cout << "Your total is " << scoresEachExercise << " out of " << totalPoints
			<< ", or " << setprecision(4)<< (percent*100) << "%." << endl;

	return 0;
}