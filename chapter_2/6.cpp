#include <iostream>
#include <stdlib.h>
#include <math.h>
#define MATH_PI 3.14

using namespace std;

int main() {
	const float y = 62.4;
	float V, r;
	float Fb;
	float pounds;
	
	cout << "Input weight: ";
	cin >> pounds;
	
	cout << "Input Radius: ";
	cin >> r;
	
	V = (4/3) * MATH_PI * pow(r,3);
	
	Fb = V * y;
	
	if(Fb >= pounds) {
		cout << "Object is floating" << endl;
	}else {
		cout << "Object is sinking" << endl;
	}
	
	
	
	return 0;
}