#include <iostream>

using namespace std;

int main() {
	
	double itemCost, interest,rate, monthly;
	
	cout << "How much does it cost for an item? ";
	cin >> itemCost;
	
	cout << "How many interest per year? in % ";
	cin >> interest;
	
	cout << "Monthly payment? ";
	cin >> monthly;
	
	rate = (interest * 0.01)/12.0;
	
	int months = 0;
	double totalInterest = 0.0;
	double debt = itemCost;
	
	
	while(debt > 0) {
		
		cout << (months + 1) << ". month:\n";
		double monthlyInterest = debt * rate;
		cout << "Interest: " << monthlyInterest << endl;
		debt += monthlyInterest;
		totalInterest += monthlyInterest;
		
		if(debt < monthly) {
			cout << "Last payment: " << debt << endl;
			debt = 0;
		}else {
			cout << "payment: " << monthly << endl;
			debt -= monthly;
		}
		
		cout << "remaining debt: " << debt << endl;
		months++;
		
	}
	
	cout << "Item paid after: " << months << endl;
	cout << "The total paid interest is: " << totalInterest << endl;
	
	
	return 0;
}