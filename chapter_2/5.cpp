#include <iostream>
#include <stdlib.h>

using namespace std;

int main() {

	for (int T = 0; T < 10; T++) {
		for (int O = 0; O < 10; O++) {
			for (int G = 0; G < 10; G++) {
				for (int D = 0; D < 10; D++) {

					if ((D == G) || (D == O) || (D == T) || (G == O)
							|| (G == T) || (O == T)) {
						//keepGoing = true;
						continue;
					}

					else if (400 * T + 40 * O + 4 * O == 1000 * G + 100 * O
							+ 10 * O + D) {
						//keepGoing = false;
						cout << "T = " << T  << endl;
						cout << "O = " << O << endl;
						cout << "G = " << G << endl;
						cout << "D = " << D << endl;
						exit(1);
					}

				}
			}

		}
	}

	return 0;
}