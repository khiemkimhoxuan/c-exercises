#include <iostream>

using namespace std;

int main() {
	double dollars;
	int chocolateBars = 0;
	int coupons = 0;
	int countChocolateCoupons = 0;

	cout << "Input dollars: ";
	cin >> dollars;

	while (dollars > 0) {

		chocolateBars++;
		coupons++;
		
		if(coupons > 6) {
			countChocolateCoupons++;
			coupons -=7;
		}
		
		dollars--;
	}
	

	cout << "Can collect chocolates: " << chocolateBars << endl;
	cout << "number of coupons to buy chocolates: " << countChocolateCoupons << endl;
	cout << "Coupons leftover: " << coupons << endl;
	return 0;
}