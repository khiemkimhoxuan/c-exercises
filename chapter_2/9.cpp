#include <iostream>
using namespace std;

int main() {
	int count = 0, number;
	double result, guess, prev_guess;

	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(5);

	cout
			<< "This program approximates the square root of a number using the Babylonian\n"
			<< "algorithm.\n"
			<< "\nInput the number of which you want the square root: ";
	cin >> number;
	cout << endl;

	guess = number / 2;
	prev_guess = number;

	while (prev_guess * .99 >= guess) {
		result = number / guess;
		prev_guess = guess;
		guess = (guess + result) / 2;
		count++;
	}

	cout << "The square root of " << number << " = " << guess << ". This took "
			<< count << " iterations.\n";

	return 0;
}
