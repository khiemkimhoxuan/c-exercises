#include <iostream>

using namespace std;

int toFahrenheit( int celsius )
{
   return ((9.0/5) * celsius + 32);
}
 
int main()
{
   int celsius;
   int fahrenheit;
 
   celsius = 100;
   while ( ( fahrenheit = toFahrenheit( celsius ) ) != celsius ) celsius--;

   std::cout << "celsius = " << celsius
             << ", fahrenheit = " << fahrenheit
             << std::endl;

   return 0;
}