#include "common.h"

date RentalInformation::getDate() {
	return date_;
}

Movie RentalInformation::getMovie() {
	return movie;
}

void RentalInformation::setDate(date newDate) {
	date_ = newDate;
}

void RentalInformation::setMovie(Movie m) {
	movie = m;
}
RentalInformation::RentalInformation(Movie m, date d) {
	movie = m;
	date_ = d;
}

vector<RentalInformation> System::getMovies() {
	return movies;
}

Account System::getAccount() {
	return account;
}


double System::getFee() {
	return fee;
}

void System::calcFee(double newFee) {
	fee += newFee;
}


void System::addMovie(Movie m, date d) {
	RentalInformation ri(m, d);
	movies.push_back(ri);
}

System::System() {
}
System::System(Account a) {
	account = a;
	fee = 0;
}