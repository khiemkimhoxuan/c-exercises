#include "common.h"





int main() {
	Store *store = new Store();
	
	//create basic accounts
	Account a1(1, "Khiem Ho Xuan");
	Account a2(2,"Test");
	Account a3(3, "Neger");
	
	
	Movie m1(1,"LA Noire","horror");
	Movie m2(2, "Ace Ventura","comedy");
	Movie m3(1,"Ninja Turtles","animation");
	
	date d1(25,8,1989);
	date d2 = store->findCurrentTime();
	date d3(1,2,2004);
	store->registerRentedMovie(a1,m1, d1);
	store->registerRentedMovie(a1,m2, d2);
	store->registerRentedMovie(a1,m3, d1);
	store->registerRentedMovie(a2,m3,d3);
	
	store->registerRentedMovie(a3,m2,d2);
	
	
	store->controlRental();
	store->printInformation();
	
	
	delete store;
	return 0;
}