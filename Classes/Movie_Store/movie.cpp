#include "common.h"

int Movie::getId() {
	return id;
}
void Movie::setId(int newId) {
	id = newId;
}

string Movie::getName() {
	return name;
}

void Movie::setName(string newName) {
	name = newName;
}

string Movie::getGenre() {
	return genre;
}

void Movie::setGenre(string newGenre) {
	genre = newGenre;
}

Movie::Movie(int i, string n, string g) {
	id = i;
	name = n;
	genre = g;
}

Movie::Movie() {

}

