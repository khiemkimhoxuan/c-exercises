#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <string>
#include <stdlib.h>
#include <vector>
#include "date.h"



class Movie {
public:
	int getId();
	void setId(int newId);
	string getName();
	void setName(string newName);
	string getGenre();
	void setGenre(string newGenre);
	
	Movie(int i, string n, string g);
	Movie();
	
private:
	int id;
	string name;
	string genre;
	
	
};

class Account {
public:
	int getId();
	void setId(int newId);
	string getName();
	void setName(string newName);
	void setRenting(bool r);
	bool getRenting();
	
	Account(int i, string n);
	Account();
	
private:
	int id;
	string name;
	bool renting;
	
	
};

class RentalInformation {
public:
	date getDate();
	Movie getMovie();
	void setDate(date newDate);
	void setMovie(Movie m);
	RentalInformation(Movie m, date d);
private:
	date date_;
	Movie movie;
	
};



class System {
public:
	vector<RentalInformation> getMovies();
	Account getAccount();
	double getFee();
	void calcFee(double newFee);
	void addMovie(Movie m, date d);
	
	System();
	System(Account a);
	
	
private:
	vector<RentalInformation> movies;
	Account account;
	date d;
	double fee;
	
	
};


class Store {
public:
	vector<System> getSystem();
	void printInformation();
	void controlRental();
	void registerRentedMovie(Account a, Movie m, date d);
	date findCurrentTime();
	
	
private:
	vector<System> systemInformation;
	
};






#endif
