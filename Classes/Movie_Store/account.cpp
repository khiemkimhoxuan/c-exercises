#include "common.h"

int Account::getId() {
	return id;
}


void Account::setId(int newId) {
	id = newId;
}

string Account::getName() {
	return name;
}
void Account::setName(string newName) {
	name = newName;
}

void Account::setRenting(bool r) {
	renting = r;
}

bool Account::getRenting() {
	return renting;
}

Account::Account(int i, string n) {
	id = i;
	name = n;
	renting = false;
}

Account::Account() {
	renting = false;
}