#include "common.h"
#include <time.h>
#include <sstream>
#include <stdio.h>
#include <algorithm>

//private functions

string convertInt(int number) {
	stringstream ss;//create a stringstream
	ss << number;//add number to the stream
	return ss.str();//return a string with the contents of the stream
}

date Store::findCurrentTime() {
	time_t theTime = time(NULL);
	struct tm *aTime = localtime(&theTime);

	int day = aTime->tm_mday;
	int month = aTime->tm_mon + 1; // Month is 0 - 11, add 1 to get a jan-dec 1-12 concept
	int year = aTime->tm_year + 1900; // Year is # years since 1900
	date d(day, month, year);
	return d;

}

vector<System> Store::getSystem() {
	return systemInformation;
}

void Store::printInformation() {
	string status;
	for (std::vector<System>::size_type i = 0; i != systemInformation.size(); i++) {
		systemInformation[i].getAccount().getRenting() ? status = "Renting"
				: status = "Not Renting anything";

		bool rentFlag = systemInformation[i].getAccount().getRenting();

		if (rentFlag) {

			cout << systemInformation[i].getAccount().getName()
					<< " Rent status: " << status << endl;
			cout << "\t\tMOVIES RENTING:" << endl;
			cout << "--------------------------------------------------"
					<< endl;
			//print out movies the person is renting;

			vector<RentalInformation> rentedList =
					systemInformation[i].getMovies();

			for (std::vector<RentalInformation>::size_type j = 0; j
					!= rentedList.size(); j++) {
				//bufferupdate, should create function for the format below
				if (rentedList[j].getDate() < findCurrentTime()) {
					char x[sizeof(date) / sizeof(date*) + 32];
					string day = convertInt(rentedList[j].getDate().day());
					string month = convertInt(rentedList[j].getDate().month());
					string year = convertInt(rentedList[j].getDate().year());
					sprintf(x, "%s:%s:%s", day.c_str(), month.c_str(),
							year.c_str());

					cout << rentedList[j].getMovie().getName()
							<< ", date rent: " << x << endl;
				}
			}
			cout << "---------------------------------------------------"
					<< endl;
			cout << "outdated sum of fee: " << systemInformation[i].getFee()
					<< "$" << endl;

		}
	}
}
void Store::controlRental() {
	string status;
	//Run rental control system
	cout << "Running control system for rental of movies" << endl;

	cout << "--------------------------------------------------" << endl;

	for (std::vector<System>::size_type i = 0; i != systemInformation.size(); i++) {
		if (systemInformation[i].getAccount().getRenting()) {

			cout << "Account name: "
					<< systemInformation[i].getAccount().getName() << endl;
			//pass if the rental is true, the person has already rented a movie
			//then iterate through rentalinformation vector to find the customer
			vector<RentalInformation> rentalcheckList =
					systemInformation[i].getMovies();

			for (std::vector<RentalInformation>::size_type j = 0; j
					!= rentalcheckList.size(); j++) {

				//check todays timestamp!

				if (rentalcheckList[j].getDate() < findCurrentTime()) {
					//oh my, the deadline has expired
					cout << " Has expired date for delivery of movie: "
							<< rentalcheckList[j].getMovie().getName()
							<< " Will get charged in dollars!" << endl;
					systemInformation[i].calcFee(500);

				}

			}

		}
		cout << "--------------------------------------------------" << endl;

	}

}
void Store::registerRentedMovie(Account a, Movie m, date d) {
	//Check if the person has been registered before
	a.setRenting(true);
	System s(a);
	bool alreadyExists = false;

	for (std::vector<System>::size_type i = 0; i != systemInformation.size(); i++) {
		if (s.getAccount().getName()
				== systemInformation[i].getAccount().getName()) {
			alreadyExists = true;
			systemInformation[i].addMovie(m, d);
		}
	}

	if (!alreadyExists) {
		s.addMovie(m, d);
		systemInformation.push_back(s);

	}

}