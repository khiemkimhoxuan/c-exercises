#include "common.h"

void createProductArray(Product prods[]) {
	
	prods[0].setPrice(250);
	prods[0].setId(1);
	prods[0].setQuantity(29);
	
	
	prods[1].setPrice(550);
	prods[1].setId(2);
	prods[1].setQuantity(20);
	
	prods[2].setPrice(2050);
	prods[2].setId(3);
	prods[2].setQuantity(100);

}


int main(int argc, char **argv) {
	Inventory *inventory = new Inventory();
	
	Product prods[3];
	createProductArray(prods);
	
	for(int i = 0; i < (sizeof(prods)/sizeof(*prods)); i++) {
		inventory->storeProduct(prods[i]);
	}
	
	//print out products
	inventory->printProducts();
	
	
	//finally do some work with the products
	
	inventory->printInventoryValue();
	
	
	
	delete inventory;
	return 0;
}