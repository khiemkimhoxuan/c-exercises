#include "common.h"

double Product::getPrice() {
	return price;
}
void Product::setPrice(double newPrice) {
	price = newPrice;
}

int Product::getId() {
	return id;
}
void Product::setId(int newId) {
	id = newId;
}

int Product::getQuantity() {
	return quantity;
}
void Product::setQuantity(int newQ) {
	quantity = newQ;
}

Product::Product(double p, int i, int q) {
	price = p;
	id = id;
	quantity = q;
}

Product::Product() {}