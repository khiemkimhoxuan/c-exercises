#include "common.h"

void Inventory::printInventoryValue() {
	
	for(std::vector<Product>::size_type i = 0; i != products.size(); i++) {
		cout << "Inventory value for: " << products[i].getId() << " : ";
		cout << (products[i].getPrice() * products[i].getQuantity()) << endl;
		
	}
	

}

void Inventory::storeProduct(Product p) {
	products.push_back(p);

}

void Inventory::printProducts() {
	for (std::vector<Product>::size_type i = 0; i != products.size(); i++) {
		cout << products[i].getId() <<": " << products[i].getPrice() << "$ quantity: " << products[i].getQuantity() << endl;
	}


}

vector<Product> Inventory::getProducts() {
	return products;
}