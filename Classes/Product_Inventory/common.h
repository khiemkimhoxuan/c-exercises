#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>

using namespace std;

class Product {
public:
	double getPrice();
	void setPrice(double newPrice);

	int getId();
	void setId(int newId);

	int getQuantity();
	void setQuantity(int newQ);

	Product(double p, int i, int q);
	Product();

private:
	double price;
	int id;
	int quantity;

};

class Inventory {

public:
	void printInventoryValue();
	void storeProduct(Product p);
	void printProducts();
	vector<Product> getProducts();

private:
	vector<Product> products;

};

#endif /* COMMON_H */
